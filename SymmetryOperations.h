#ifndef _SYMMETRYOPERATIONS_H
#define _SYMMETRYOPERATIONS_H

#include <string>
#include <vector>

#include "Direction.h"

using namespace std;



struct SymmetryOperations {
	string name;
	Directions directions;
};

#endif

#ifndef _INTERACTIONS_H
#define _INTERACTIONS_H

#include <string>
#include <vector>

using namespace std;

#include "Direction.h"

struct Interaction {
	string name;
	Directions directions;
	int multiplicity;
	double energy;
	Interaction(const string name = "", int multiplicity = 0, double energy = 0.0) : 
		name(name), multiplicity(multiplicity), energy(energy) {};
};

class Interactions : public vector<Interaction> {
	friend ostream& operator<<(ostream& output, const Interactions&);

};

#endif

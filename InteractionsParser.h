#ifndef _INTERACTIONSPARSER_H
#define _INTERACTIONSPARSER_H

#include "Interactions.h"
#include "SimpleIni.h"

class InteractionsParser {
public:
	InteractionsParser(const char *ini_filename);
	Interactions getInteractions();

private:
	Interactions _interactions;
	CSimpleIni _ini;

	void parseInteraction(CSimpleIni::TNamesDepend::const_iterator,
			CSimpleIni::TNamesDepend::const_iterator);
};

#endif

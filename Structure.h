#ifndef STRUCTURE_H_
#define STRUCTURE_H_

#include <blitz/array.h>
using namespace blitz;

#include "SymmetryOperations.h"
#include "Lattice.h"
#include "Interactions.h"


using namespace std;


class Structure {
public:

	string structureName;
	Lattice *lattice;
	Interactions interactions;
	// to be read in from structure ini file... if not present -> new structure!!
	double dftEnergy; 
	bool newStructure;
	double onSiteEnergy;
	// vector<double> onSiteEnergies;
	
	// Cluster_Expansion cluster_expansion;	
	// this is the data object to store the algebraic sum which is a combination of
	// number_of_CO * adsorbate_on_site_energy(CO) + interaction(1).energy *
	//  interaction(1).multiplicity ... etc.

	// Structure(): name(""), lattice(), interactions(), dft_energy(0) { };

	double getEnergy(void);
};


class Structures : public vector<Structure> {
// anything needed here?

// methode: new_structure()
// und dann je nach parameter: random.. oder alles an CO positionen durchprobieren etc..
// oder vielleicht sogar per input file einlesen...
	

};


#endif /*STRUCTURE_H_*/

#include "InteractionsParser.h"
#include "StringTokenizer.h"

#include <algorithm>
#include <iostream>
#include <stdexcept>

InteractionsParser::InteractionsParser(const char *ini_filename) 
	: _ini(false, true, false)
{
	SI_Error rc = _ini.LoadFile(ini_filename);
	if (rc < 0) 
		throw runtime_error("Failed loading interaction ini file: " +
				CSimpleIni::str_SI_Error(rc));
		
	const CSimpleIni::TKeyVal *section = _ini.GetSection("Interactions");
	if (!section)
		throw runtime_error("Interactions ini file is missing "
				"'Interactions' section.");
		
	CSimpleIni::TNamesDepend interactionTypes;
	_ini.GetAllKeys("Interactions", interactionTypes);

	for (CSimpleIni::TNamesDepend::const_iterator key = 
			interactionTypes.begin(); 
			key != interactionTypes.end(); ++key) {

		CSimpleIni::TNamesDepend interactions;
		_ini.GetAllValues("Interactions", key->pItem, interactions);

		for (CSimpleIni::TNamesDepend::const_iterator value = 
				interactions.begin();
				value != interactions.end(); ++value) {
			parseInteraction(key, value);

		}
	}
}

void InteractionsParser::parseInteraction(
		CSimpleIni::TNamesDepend::const_iterator key,
		CSimpleIni::TNamesDepend::const_iterator value)
{
	StringTokenizer strtok = StringTokenizer(value->pItem, ",");
	int cnt = strtok.countTokens();
	if (cnt == 0)
		throw runtime_error("Empty \"" + string(key->pItem) 
				+ "\" key.");
	Interaction interaction;
	interaction.name = "";

	for (int i = 0; i < cnt; i++) {
		double x_frac, y_frac; // interactions may be given as absolute coordinates and scaled to unit cell size later!
		string tmp = strtok.nextToken();
		if (sscanf(tmp.c_str(), "%lf/%lf", &x_frac, &y_frac) != 2 ||
				x_frac < 0 || y_frac < 0) {
			// Treat last tuple as comment
			if (i==cnt-1)
				interaction.name = tmp;
			else
				throw runtime_error("Skrewed fraction coordinates: " +
						string(tmp));
		}
		interaction.directions.push_back(
				Direction(x_frac, y_frac));
	}
	// TODO: check cardinality (trio, etc...)
	_interactions.push_back(interaction);
}

Interactions InteractionsParser::getInteractions()
{
	// FIXME: auto_ptr
	return _interactions;
}
	

#ifndef STRUCTUREPARSER_H_
#define STRUCTUREPARSER_H_

#include "Structure.h"
#include "SimpleIni.h"

#include "Lattice.h"
#include "LatticeParser.h"

#include "Interactions.h"
#include "InteractionsParser.h"

#include "SymmetryOperations.h"

class StructureParser {
public:
	StructureParser(const char *ini_filename, const char *ini_filename_interactions);
	Structure getStructure();

private:
	Structure _structure;
	CSimpleIni _ini_structure, _ini_interactions;
};

#endif /*STRUCTUREPARSER_H_*/

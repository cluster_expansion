#include <stdexcept>

#include <blitz/array.h>
using namespace blitz;

#include "SymmetryOperations.h"
#include "Lattice.h"
#include "Interactions.h"
#include "Structure.h"

using namespace std;


double
Structure::getEnergy()
{
	// FIXME: check adsorbate species and get correct on-site energy contribution

	// get interaction multiplicities for current configuration
	lattice.assessInteractions(interactions);
	
	double energy = 0.0;
	
	for (LatticeLayer::iterator i = lattice.adsorbates.begin(); i != lattice.adsorbates.end(); i++) {
		if (lattice.adsorbates(i.position()) == empty)
			continue;
		// implement species checking CO or O or whatever
		energy += onSiteEnergy; // FIXME : adsorbate_on_site_energy(species specific!)
		
		for (Interactions::iterator interaction = interactions.begin();
				interaction != interactions.end(); interaction++) {
				energy += (interaction->energy * interaction->multiplicity);
				};
	};
			 
	return energy;
}


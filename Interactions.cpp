#include <stdexcept>
#include <string>
#include <iostream>

#include "Interactions.h"
#include "SimpleIni.h"

using namespace std;

ostream& operator<<(ostream& output, const Interactions& interactions)
{
	for (Interactions::const_iterator interaction = interactions.begin();
			interaction != interactions.end(); interaction++) {
		output << (interaction->name.empty() ? "(no name)" : interaction->name)
			<< " [M:" << interaction->multiplicity 
			<< ", E:" << interaction->energy 
			<< "]\t\t: ";
		for (Directions::const_iterator direction = interaction->directions.begin();
				direction != interaction->directions.end(); direction++) {
			output << "(" << direction->x << "/" << direction->y << ") ";
		}
		output << endl;
	}
	return output;
}

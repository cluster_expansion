#ifndef _LATTICE_H
#define _LATTICE_H

#include <blitz/array.h>
using namespace blitz;

#include "SymmetryOperations.h"
#include "Interactions.h"

enum Occupation { 
	empty = 0,
	CO,
	Pd,
	O,
	max_species
};

class LatticeLayer : public Array<Occupation, 2> {
public:
	LatticeLayer& operator=(Occupation const& o);

	friend ostream& operator<<(ostream& output, const LatticeLayer&);
};


class Lattice {
public:
	Lattice();
	Lattice(int, int, int, int);

	int GranularityX, GranularityY;
	int UnitCellSizeX, UnitCellSizeY;

	LatticeLayer adsorbates;
	LatticeLayer surface;

	SymmetryOperations _symmetryOperations;

	void assessInteractions(Interactions&);

	friend ostream& operator<<(ostream& output, const Lattice&);

};

#endif

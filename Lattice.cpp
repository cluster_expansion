#include <stdexcept>

#include "Lattice.h"
#include "SimpleIni.h"

// FIXME: LatticeType should derive operator= from Array<Occupation, 2>
LatticeLayer& LatticeLayer::operator=(Occupation const& o) {
	for (LatticeLayer::iterator i = this->begin(); i != this->end(); ++i)
		this->operator()(i.position()) = o;
	return *this;
};

ostream& operator<<(ostream& output, const LatticeLayer& layer)
{
	for (int y = 0; y < layer.extent(secondDim); ++y) {
		for (int x = 0; x < layer.extent(firstDim); ++x) {
			switch (layer(x,y)) {
			case empty: 	cout << "."; break;
			case CO:	cout << "C"; break;
			case Pd:	cout << "P"; break;
			default: 	throw out_of_range("Invalid occupation");
			}
		}
		output << endl;
	}
	
	return output;
}

Lattice::Lattice(int UnitCellSizeX, int UnitCellSizeY, int GranularityX, int GranularityY) :
	GranularityX(GranularityX), GranularityY(GranularityY), 
	UnitCellSizeX(UnitCellSizeX), UnitCellSizeY(UnitCellSizeY)
{ 
	TinyVector<int, 2> extent(UnitCellSizeX * GranularityX, UnitCellSizeY * GranularityY);
	adsorbates.resize(extent);
	surface.resize(extent);
}

void
Lattice::assessInteractions(Interactions &interactions)
{
	for (LatticeLayer::iterator i = adsorbates.begin(); i != adsorbates.end(); i++) {
		if (adsorbates(i.position()) == empty)
			continue;
		cout << "POS" << i.position() << endl;
		for (Interactions::iterator interaction = interactions.begin();
				interaction != interactions.end(); interaction++) {
			cout << "checking " << interaction->name << ": ";
			for (Directions::iterator direction = interaction->directions.begin();
					direction != interaction->directions.end(); direction++) {
				for (Directions::iterator symmetry_operation = _symmetryOperations.directions.begin();
					symmetry_operation != _symmetryOperations.directions.end(); symmetry_operation++) {

					int x = (i.position()[firstDim] + (int)((direction->x / UnitCellSizeX)*
								(symmetry_operation->x * GranularityX)) %
								(UnitCellSizeX * GranularityX) );
					int y = (i.position()[secondDim] + (int)((direction->y / UnitCellSizeY)*
								(symmetry_operation->y * GranularityY)) %
								(UnitCellSizeY * GranularityY));
					cout << "(" << x << "/" << y << ") ";
					if (adsorbates(x, y) != empty) {
						interaction->multiplicity++;
						cout << "HIT ";
						}
				}
			}
			cout << endl;
		}
	}
}


ostream& operator<<(ostream& output, const Lattice& lattice)
{
#define OUTPUT(l) \
	for (int y = 0; y < l.extent(secondDim); ++y) { \
		if (y && y % lattice.GranularityY == 0) { \
			for (int i = 0; i < lattice.UnitCellSizeX * \
					(lattice.GranularityX+1)-1; ++i) { \
				if (i && (i+1) % (lattice.GranularityX+1) == 0) output << "+"; \
				else output << "-"; \
			} \
			output << endl; \
		} \
		for (int x = 0; x < l.extent(firstDim); ++x) { \
			if (x && x % lattice.GranularityX == 0) output << "|"; \
			switch (l(x,y)) { \
			case empty: 	cout << "."; break; \
			case CO:	cout << "C"; break; \
			case Pd:	cout << "P"; break; \
			default: 	throw out_of_range("Invalid occupation"); \
			} \
		} \
		output << endl; \
	}
	

	output << "lattice: UnitCellSizeX = " << lattice.UnitCellSizeX
		<< ", UnitCellSizeY = " << lattice.UnitCellSizeY
		<< ", GranularityX = " << lattice.GranularityX
		<< ", GranularityY = " << lattice.GranularityY << endl
		<< endl << "surface: " << endl;
	OUTPUT(lattice.surface);
	output << endl << "adsorbates: " << endl ;
	OUTPUT(lattice.adsorbates);
	output << endl;

	return output;
}

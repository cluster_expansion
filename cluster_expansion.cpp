/*
 * Determination of lateral interaction parameters on a c(2x2) square lattice
 * using linear combinations to express each interaction and then counting 
 * them in terms of cluster-expansion coefficients
 *
 * Michael Rieger, FHI, 2008
 * rieger@fhi-berlin.mpg.de
 *
 */

#include <iostream>
#include <memory>


#include "Lattice.h"
#include "LatticeParser.h"

#include "Interactions.h"
#include "InteractionsParser.h"

#include "SymmetryOperations.h"


#include "Structure.h"
#include "StructureParser.h"



void usage(const char *prog)
{
	cerr << "usage: " << prog 
		<< " structure-ini-file interactions-ini-file" 
		<< endl;

	exit(EXIT_FAILURE);
}


int main(int argc, char* argv[])
{
	if (argc != 3)
		usage(*argv);

	try {
		
		cout << "parsing lattice" << endl;
		LatticeParser latticeParser(argv[1]);
		Lattice *lattice = latticeParser.getLattice();
		cout << *lattice;
	
		cout << "parsing interactions" << endl;
		InteractionsParser interactionsParser(argv[2]);
		Interactions interactions = interactionsParser.getInteractions();

		cout << "interactions (before assessment):" << endl << interactions << endl;
		lattice->assessInteractions(interactions);
		cout << "interactions (after assessment):" << endl << interactions;
		
		
		StructureParser structureParser(argv[1], argv[2]);
		Structure structure = structureParser.getStructure();
		
		cout << structure.lattice << endl;
		cout << structure.onSiteEnergy << endl;
	}
	catch (exception& e) {
		cerr << "FATAL: " << e.what() << endl;
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}
